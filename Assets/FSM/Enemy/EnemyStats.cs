﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/EnemyState")]
public class EnemyStats : ScriptableObject
{
    public float attackRange;
    public float attackRate;

    public float lookRange;
    public float lookSphereCastRadius;

    public float searchingTurnSpeed;
    public float searchDuration;
}